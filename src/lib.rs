//! Allows to reuse notifications created by `notify-rust` , replacing the contents
//! of an already existing notification, instead of creating a new one.
//!
//! # Examples
//!
//! ## Example for a single notification
//!
//! ```no_run
//! use notify_rust::*;
//! use reuse_notification::ReuseNotification;
//!
//! Notification::new()
//!     .summary("Firefox News")
//!     .body("This will almost look like a real firefox notification.")
//!     .icon("firefox")
//!     .timeout(Timeout::Milliseconds(6000))
//!     .reuse() // <-- instead of `show()`
//!     .unwrap();
//! ```
//!
//! ## Example for different reusable notifications
//!
//! In order to overwrite a specific notification, provide a string to `.reuse()`.
//!
//! Future calls to `.reuse()` with the same string will replace the contents
//! of the old notification with the new ones in the new notification instance.
//!
//! ```no_run
//! use notify_rust::*;
//! use reuse_notification::ReuseNotification;
//!
//! Notification::new()
//!     .summary("Firefox News")
//!     .body("This will almost look like a real firefox notification.")
//!     .icon("firefox")
//!     .timeout(Timeout::Milliseconds(6000))
//!     .reuse("firefox_notification") // <-- instead of `show()`
//!     .unwrap();
//!
//! Notification::new()
//!     .summary("Other News")
//!     .body("This will almost look like a real firefox notification.")
//!     .icon("firefox")
//!     .timeout(Timeout::Milliseconds(6000))
//!     .reuse("other_notification") // <-- instead of `show()`
//!     .unwrap();
//!
//! Notification::new()
//!     .summary("Firefox News 2")
//!     .body("This will reuse the previous 'firefox notification'.")
//!     .icon("firefox")
//!     .timeout(Timeout::Milliseconds(6000))
//!     .reuse("firefox_notification") // <-- instead of `show()`
//!     .unwrap();
//! ```
//!

extern crate notify_rust;
extern crate rand;

use notify_rust::error::Result;
use notify_rust::Notification;
use notify_rust::NotificationHandle;
use rand::Rng;
use std::env::temp_dir;
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;


/// Allows to reuse notifications created via `notfy-rust`.
pub trait ReuseNotification {
    fn reuse(&mut self) -> Result<NotificationHandle>;

    fn reuse_with(&mut self, identifier: &str) -> Result<NotificationHandle>;
}

impl ReuseNotification for Notification {
    fn reuse(&mut self) -> Result<NotificationHandle> {
        let internal_id = self.appname.clone();
        ReuseNotification::display_reusing_id(self, &internal_id)
    }

    fn reuse_with(&mut self, identifier: &str) -> Result<NotificationHandle> {
        let mut internal_id = self.appname.clone();
        internal_id.push_str(identifier);
        ReuseNotification::display_reusing_id(self, &internal_id)
    }
}

impl ReuseNotification {
    fn display_reusing_id(notification: &mut Notification, file_suffix: &String) -> Result<NotificationHandle> {
        let temp_file = temp_file(file_suffix);
        let stored_id = stored_id(&temp_file);
        notification.id(stored_id)
            .show()
            .map(|notification_handle| {
                // Sometimes the id set is ignored by the notification server.
                // Let's overwrite the stored notification id if does not match the server's.
                if notification_handle.id() != stored_id {
                    store_new_notification_id(&temp_file, notification_handle.id());
                }
                notification_handle
            })
    }
}

fn temp_file(suffix: &String) -> PathBuf {
    let mut file_name = "notification_id_".to_owned();
    file_name.push_str(suffix);
    let mut temp_file = temp_dir();
    temp_file.push(file_name);
    temp_file
}

fn stored_id(temp_file: &PathBuf) -> u32 {
    if temp_file.exists() {
        read_notification_id(temp_file)
    } else {
        store_new_notification_id(temp_file, random_id())
    }
}

fn read_notification_id(temp_file: &PathBuf) -> u32 {
    match File::open(temp_file) {
        Ok(mut file) => {
            let mut s = String::new();
            match file.read_to_string(&mut s) {
                Ok(_) => s.parse::<u32>().unwrap_or(fallback()),
                Err(_) => fallback()
            }
        }
        Err(_) => fallback()
    }
}

fn store_new_notification_id(temp_file: &PathBuf, id: u32) -> u32 {
    File::create(temp_file)
        .and_then(|mut file| file.write_all(id.to_string().as_bytes()).map(|_| id))
        .unwrap_or(fallback())
}

// If there are any problems while reading or writing the notification id to disk,
// we'll use a fallback in order not to disrupt the rest of the application
fn fallback() -> u32 {
    random_id()
}

fn random_id() -> u32 {
    let mut rng = rand::thread_rng();
    rng.gen::<u32>()
}
